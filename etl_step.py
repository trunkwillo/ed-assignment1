import psycopg2
import mpu
from datetime import datetime

try:
    connection = psycopg2.connect(user="postgres",
                                  password="1234",
                                  host="127.0.0.1",
                                  port="5432",
                                  database="postgres")
    cursor = connection.cursor()

    # Getting all the Information from temporary table
    # Get the distinct taxi id's
    query = 'SELECT DISTINCT taxiid FROM assignment1."Temporary_Data" ORDER BY taxiid;'
    cursor.execute(query)
    distinct_ids = cursor.fetchall()

    # Iterate over all distinct ID's
    for i in distinct_ids:
        query = 'SELECT segmentid, lat, lon, dateobs FROM assignment1."Temporary_Data" WHERE taxiid = {} ORDER BY dateobs ASC'.format(i[0])
        cursor.execute(query)
        rows = cursor.fetchall()

        for r in range(len(rows)-1):
            # Calcular velocidade:
                # Calcular distância
            dist = mpu.haversine_distance((rows[r][1], rows[r][2]), (rows[r+1][1], rows[r+1][2])) # In Km's
                # Calcular o intervalo de tempo
            time_delta = (rows[r+1][3] - rows[r][3]).total_seconds() / 3600.0
                # Cálculo final
            if dist == 0 or time_delta == 0:
                continue
            instant_speed = dist/time_delta
            #print(str(instant_speed) + " Km/h")
            if instant_speed < 200.0:
            # Adicionar à tabela temporária (segmentid, data, taxi_id, velocidade)
                query = "INSERT INTO assignment1.\"Temporary_Fact_Table\"(segment, dateobs, taxiid, instant_speed) VALUES ({},'{}',{},{});".format(rows[r][0],rows[r][3],i[0],instant_speed)
                cursor.execute(query)
            else:
                print("Removed non-sense value:" + str(instant_speed))


    # Calculating the instant speed per segment and adding to facts table (requires select positions and timestamp per taxi ordered by time)

    # Writing result to tables
    connection.commit()

except (Exception, psycopg2.Error) as error :
    print ("Error while processing data from PostgreSQL: ", error)

finally:
    #closing database connection.
    if(connection):
        cursor.close()
        connection.close()
        print("PostgreSQL connection is closed")
