import psycopg2
import mpu
from datetime import datetime

try:
    connection = psycopg2.connect(user="postgres",
                                  password="1234",
                                  host="127.0.0.1",
                                  port="5432",
                                  database="postgres")
    cursor = connection.cursor()

    # Getting all the Information from temporary table
    # Get the distinct taxi id's
    query = 'SELECT DISTINCT taxiid FROM assignment1."Temporary_Data" ORDER BY taxiid;'
    cursor.execute(query)
    distinct_ids = cursor.fetchall()

    # Iterate over all distinct ID's
    for i in distinct_ids:
        query = 'SELECT segmentid, lat, lon, dateobs FROM assignment1."Temporary_Data" WHERE taxiid = {} ORDER BY dateobs DESC LIMIT 1'.format(i[0])
        cursor.execute(query)
        rows = cursor.fetchall()

        # Adicionar à tabela temporária (segmentid, data, taxi_id, velocidade)
        query = "INSERT INTO assignment1.\"Delta_Table\"(segment, dateobs, taxiid, lat, lon) VALUES ({},'{}',{},{}, {});".format(rows[0][0],rows[0][3],i[0],rows[0][1],rows[0][2])
        cursor.execute(query)


    # Calculating the instant speed per segment and adding to facts table (requires select positions and timestamp per taxi ordered by time)

    # Writing result to tables
    connection.commit()

except (Exception, psycopg2.Error) as error :
    print ("Error while processing data from PostgreSQL: ", error)

finally:
    #closing database connection.
    if(connection):
        cursor.close()
        connection.close()
        print("PostgreSQL connection is closed")
